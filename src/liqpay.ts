import crypto from 'crypto'

export interface LiqPayAPI{
    public_key?: string,
    version: '1' | '2' | '3',
    action?         : 'pay' | 'hold' | 'subscribe' | 'status',
    amount?         : number,
    currency?       : 'USD' | 'UAH' | 'RUB' | 'EUR'
    description?    : string,
    order_id?       : number | string,
    result_url?     : string
}

export class LiqPay{
    private host = 'https://www.liqpay.ua/api/'

    constructor(
        private public_key: string,
        private private_key: string
    ){}
    formateDataAndSignature(paramsMinifed: Omit<LiqPayAPI, 'public_key'>){
        const paramsJSON = JSON.stringify({
            ...paramsMinifed,
            public_key: this.public_key
        })

        const data = Buffer.from(paramsJSON).toString('base64')
        const sha1 = crypto.createHash('sha1')
        sha1.update(this.private_key+data+this.private_key)
        const signature = sha1.digest('base64')
        
        return {data, signature}
    }
    getButtonHTML(paramsMinifed: Omit<LiqPayAPI, 'public_key'>){
        const {data, signature} = this.formateDataAndSignature(paramsMinifed) 
        return `
        <form method="POST" action="https://www.liqpay.ua/api/${paramsMinifed.version}/checkout" accept-charset="utf-8">
         <input type="hidden" name="data" value="${data}"/>
         <input type="hidden" name="signature" value="${signature}"/>
         <input type="image" src="https://static.liqpay.ua/buttons/p1ru.radius.png"/>
        </form>
        `
    }    
}
//
