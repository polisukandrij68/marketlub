import React from 'react'
import { useParams } from 'react-router'
import checkMark from '../asset/checkmark.svg'

interface GreatingPageProps {
    successfullProp?: boolean
}

export const GreatingPage: React.FC<GreatingPageProps> = ({successfullProp}) => {
    interface StylesContainer{
        [key: string]: React.CSSProperties
    }
    const params = useParams<{successfull: string}>()
    
    const successfull = successfullProp ? successfullProp : params.successfull === 'success' 
    const styles: StylesContainer = {
        main:{
            backgroundColor: successfull ? '#DCEDC8' : '#FFCDD2',
            width: '100vw',
            height: '100vh',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
        },
        greatinBody: {
            backgroundColor: '#fff',
            padding: 20,
            borderRadius: 9,
            textAlign: 'center'
        },
        infoIMG: {
            backgroundColor: successfull ? '#4CAF50' : '#F44336',
            width: 100,
            height: 100,
            padding: 30,
            borderRadius: '50%',
            fontSize: 100,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            color: '#fff',
            margin: '0 auto',
            marginBottom: 30,
        }
    }
    return(
        <div className="main" style={styles.main}>
            <div className="greating-body" style={styles.greatinBody}>
                {successfull ? <img src={checkMark} style={styles.infoIMG} alt="Успіх"/> : <p style={styles.infoIMG}>&times;</p>}
                <p>Платіж проведено {!successfull && 'не'} успішно!</p><br/>
                <a href="/login">Повернутися до кабінету</a>
            </div>
        </div>
    )
}