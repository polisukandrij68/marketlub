import React from 'react'
import { Link } from 'react-router-dom'
import logoIMG from '../asset/logo.svg'
import '../styles/header.css'


export const AppHeader: React.FC = () => {
    return(
        <section className="header-info">
                <div className="logo">
                    <img src={logoIMG} alt="logo"/>
                </div>
                <Link className="header-link" to={'/about'}>Про нас</Link>
                <Link className="header-link" to={'/confidentiality'}>Умови використання</Link>
            </section>
    )
}