import axios from 'axios'
import md5 from 'md5'

export class DB {
    static host: string = 'http://marketlub.pp.ua/api'
    // static host: string = 'http://localhost:9000/api'
    static async getNews(){
        try {
            const {data} = await axios.get(this.host+'/get/news')
            return {type: 'success', data}  
        } catch {
            return {type: 'error'}
        }
    }
    static async setNews(text: string | undefined){
        const TOKEN = localStorage.getItem('TOKEN')
        if(!text) return
        try {
            await axios.get(this.host+"/set/news", {
                params: {
                    text
                },    
                headers: {
                    Authorization: "Barier "+TOKEN
                } 
            })
            return {type: 'success'}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async loginClient(id: string, password: string){

        try {
            const {data} = await axios.get(this.host+'/get/user/'+id, {
                params: { password : password }
            })
            //@ts-ignore
            return {type: "success", price1: data.price1, price2: data.price2, name: data.name, data}
        } catch (error) {
            return {type: 'error'}
        }
    }
    static async setPayAction(id: string, type: '1' | '2' | '1plus2'){
        try {
            await axios.get(this.host+'/set/action/'+id, {
                params: {order_action: type}
            })
            return {type: "success"}
        } catch (error) {
            return {type: 'error'}
        }
    }
    static async loginAdmin(login: string, password: string){
        try {
            const {data} = await axios.get(this.host+'/admin/login', {params: {login, password: md5(password)}})
            return {type: "success", token: data.token}
        } catch (error) {
            return {type: "error"}
        }
    }
    static async getList(offset: number){
        const TOKEN = localStorage.getItem('TOKEN')
        
        try {
            const {data} = await axios.get(this.host+"/list", {
                params: {offset},
                headers: {
                    Authorization: "Barier "+TOKEN
                } 
            })
            return {type: 'success', data: data.data}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async getOrderList(id: string){
        try {
            const {data} = await axios.get(this.host+`/get/orders/${id}`, {})
            return {type: 'success', data: data.data}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async getCount(){
        const TOKEN = localStorage.getItem('TOKEN')
        
        try {
            const {data} = await axios.get(this.host+"/count", {
                headers: {
                    Authorization: "Barier "+TOKEN
                } 
            })
            return {type: 'success', count: data.count}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async create(name: string, id: string, price1: number, price2: number){
        const TOKEN = localStorage.getItem('TOKEN')
        
        try {
            await axios.get(this.host+"/create", {
                params: {
                    name,
                    id,
                    price1, price2
                },
                headers: {
                    Authorization: "Barier "+TOKEN
                } 
            })
            return {type: 'success'}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async getListByQuery(query: string){
        const TOKEN = localStorage.getItem('TOKEN')
        
        try {
            const {data} = await axios.get(this.host+"/list/"+query, {
                headers: {
                    Authorization: "Barier "+TOKEN
                } 
            })
            return {type: 'success', data: data.data}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async getSum(type: number){
        const TOKEN = localStorage.getItem('TOKEN')
        
        try {
            const {data} = await axios.get(this.host+"/sum/"+type, {
                headers: {
                    Authorization: "Barier "+TOKEN
                } 
            })
            return {type: 'success', sum: data.sum}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async setPrice(value: string, id:number, type: 1 | 2){
        const TOKEN = localStorage.getItem('TOKEN')

        try {
            await axios.get(this.host+"/set/price/"+type, {
                headers: {
                    Authorization: "Barier "+TOKEN
                },
                params: {
                    value,
                    id
                }
            })
             
            return {type: 'success'}
        } catch (error) {
            return {type: 'error'}            
        }
    }
    static async downloadEXCEL() {
        const TOKEN = localStorage.getItem('TOKEN')
        
        axios({
            url: this.host+"/excel",
            method: "GET",
            responseType: 'blob',
            headers: {
                Authorization: "Barier "+TOKEN
            }
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'dump.xlsx'); //or any other extension
            document.body.appendChild(link);
            link.click();
            link.remove()
        })
    }
}